OUTPUT=output
TARGET=novel

all:
	docker run -u `id -u`:`id -g` --rm -v `pwd`:/workdir lualatex lualatex ${TARGET}.tex

clean:
	rm -rf *~ *.log *.aux ${OUTPUT}

convert:
	mkdir -p ${OUTPUT}
	convert -density 600 ${TARGET}.pdf ${OUTPUT}/${TARGET}.png
	mogrify -background white -gravity center -extent 4961x7016 ${OUTPUT}/*.png