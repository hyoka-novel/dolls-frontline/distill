# The Secret Key Certification

## Requires
- lualatex : See https://gitlab.com/hyoka/lualatex
- ImageMagick : Clipstudio に組み込むために使用
    - Policy を変更しないと`make convert` に失敗することがある (See https://qiita.com/atuyosi/items/b782ab2130570b72aa93)

## Build

```bash
make # create pdf document
make convert # create png images
```

## Publishications
- ポチの部下 @少女戦略最前線02 け-05 

&copy; 2019 hyoka.
